import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod, Request } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class MetadataService {
  //private host = 'http://localhost:8092';
  private host = 'http://ec2-52-91-169-151.compute-1.amazonaws.com:8092';
  private url = this.host + '/metadatas';
  private header = new Headers();
  private data = new RequestFormat();
  private colFormat = new ColFormat();

  constructor(public http: Http) { }

  addDataset(dataset){
    console.log(this.url);
    console.log(dataset);
    this.data.busDatasetName = dataset.name;
    this.data.dataset_status = dataset.status;
    this.data.datasetPath = dataset.path;
    this.data.targetType = dataset.type;
    console.log(this.data);
    this.header.append('Content-Type', 'application/json;charset-utf-8');
    let options = new RequestOptions({
      headers: this.header,
      url: this.url,
      method: RequestMethod.Post,
      body: this.data
    });
    return this.http.request(new Request(options)).map((res: Response) => {
      if(res){
        return { status: res.status, json:res.json()}
      }
    })
  }

  addDatasetColumn(files: File[]){
    let uploadUrl = this.host + "/csv/upload";
    let file: File = files[0];
    let formData: FormData = new FormData();
    formData.append('file', file, file.name);
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({headers: headers})
    return this.http.post(uploadUrl, formData, options)
  }

  addColumns(columns){
    //console.log(columns);
    this.colFormat.busDatasetName = columns.bus_dataset_name;
    this.colFormat.columnName = columns.column_name;
    this.colFormat.columnDataType = columns.column_data_type;
    this.colFormat.columnDesc = columns.column_desc;
    this.colFormat.columnNpiInd = columns.column_npi_ind;
    this.colFormat.columnPciInd = columns.column_pci_ind;
    console.log(this.colFormat);
    let col_url = this.host + '/metadataCols';
    let header = new Headers();
    header.append('Content-Type', 'application/json;charset-utf-8');
    let options = new RequestOptions({
      headers: header,
      url: col_url,
      method: RequestMethod.Post,
      body: this.colFormat
    });
    return this.http.request(new Request(options));
  }

  getDatasets(){
    return this.http.get(this.url);
  }

  getColumns(dataset_name){
    console.log('getting column data');
    console.log(dataset_name);
    let get_col_url = this.host + '/metadataCols/search/findByBusDatasetName?dataset='+dataset_name
    return this.http.get(get_col_url);
  }

  deleteDataset(url){
    return this.http.delete(url).map((res: Response) => {
      if(res){
        return {status: res.status, json: res.json()}
      }
    })
  }
}

export class RequestFormat{
  busDatasetName: string;
  targetType: string;
  datasetPath: string;
  dataset_status: string;
  constructor(){}
}

export class ColFormat{
  busDatasetName: String;
  columnName: String;
  columnDesc: String;
  columnDataType: String;
  columnNpiInd: String;
  columnPciInd: String;
  constructor(){}
}
