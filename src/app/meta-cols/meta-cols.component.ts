import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-meta-cols',
  templateUrl: './meta-cols.component.html',
  styleUrls: ['./meta-cols.component.css']
})
export class MetaColsComponent implements OnInit {
  @Input() columns: any;
  @Input() dispInd: String;
  constructor() { }
  ngOnInit() {
    console.log("Inside columns component");
    console.log(this.dispInd);
    console.log(this.columns);
  }
}
