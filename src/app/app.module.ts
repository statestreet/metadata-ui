import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MetaStatusComponent } from './meta-status/meta-status.component';
import { MetaAddComponent } from './meta-add/meta-add.component';
import { MetadataService } from './metadata.service';
import { SpinnerComponent } from './spinner/spinner.component';
import { MetaColsComponent } from './meta-cols/meta-cols.component';

@NgModule({
  declarations: [
    AppComponent,
    MetaStatusComponent,
    MetaAddComponent,
    SpinnerComponent,
    MetaColsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MetadataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
