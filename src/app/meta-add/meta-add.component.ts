import { Component, OnInit } from '@angular/core';
import { MetadataService } from '../metadata.service';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-meta-add',
  templateUrl: './meta-add.component.html',
  styleUrls: ['./meta-add.component.css'],
  providers: [MetadataService]
})

export class MetaAddComponent implements OnInit {
  private values: Dataset;
  public register: any;
  public columsInfo: any;
  public showColTable: boolean;
  public datasetSuccessInd: boolean;
  public columnSuccessInd: boolean;
  public errorInd: boolean;
  public errorText: String;
  public showSpinner: boolean = false;
  public resp = { status: 0, json: "" };
  constructor(public service: MetadataService) {
    this.values = new Dataset()
    this.showColTable = false;
    this.datasetSuccessInd = false;
    this.columnSuccessInd = false;
    this.errorInd = false;
  }

  ngOnInit() {

  }

  onRegister(form) {
    this.showSpinner = true;
    console.log(this.values);
    this.values.status = "New";
    this.service.addDataset(this.values).subscribe(response => {
      if (response.status == 201) {
        this.datasetSuccessInd = true;
        console.log(response);
        console.log('Registering columns')
        let len = this.columsInfo.length;
        let count = 0;
        for (let col of this.columsInfo) {
          count = count + 1;
          this.service.addColumns(col).subscribe((data) => {
            if (data.ok) {
              if(count >= len){
                this.showSpinner = false;
                this.resp.status = 201;
              }
              console.log('Column loaded successfully')
            } else {
              if(count >= len){
                this.showSpinner = false;
                this.resp.status = 999;
              }
              this.errorInd = true;
              this.errorText = "Error Registering all the columns. Please contact Ingestion team!!"
              console.log('Error registering column ' + col.column_name)
            }
          }, (err) => {
            if(count >= len){
                this.showSpinner = false;
                this.resp.status = 999;
            }
            console.log(err);
          })
        }
      } else {
        this.errorText = "Error Registering dataset. Column Registration skipped!!";
      }
    }, err => {
      this.errorText = "Error Registering dataset. Column Registration skipped!!"
    })
  }

  onRemoveStatus() {
    this.values.initialize();
    this.resp = { status: 0, json: "" };
    this.columsInfo = null;
    this.showColTable = false;
  }

  fileUpload(file: any) {
    console.log('Uploading file')
    console.log(file);
    let files = file.files;
    this.service.addDatasetColumn(files)
      .map(data => data.json())
      .subscribe((data) => {
        console.log(data)
        this.columsInfo = data;
        console.log("Success");
        this.showColTable = true;
      });
  }
}

export class Dataset {
  name: string;
  type: string;
  path: string;
  status: string;
  constructor() {

  }
  initialize() {
    this.name = "";
    this.path = "";
    this.status = "";
    this.type = "";
  }
}