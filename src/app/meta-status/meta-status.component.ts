import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MetadataService } from '../metadata.service';

@Component({
  selector: 'app-meta-status',
  templateUrl: './meta-status.component.html',
  styleUrls: ['./meta-status.component.css']
})
export class MetaStatusComponent implements OnInit {
  private hideTable: boolean;
  private errorText: string;
  private datasets: any;
  public columnDetail: any;
  public modalHeader: any;
  private showMe: string;
  constructor(private service: MetadataService) { }
  private hideDeleteSpinner: boolean = true;
  ngOnInit() {
    this.showMe = "";
    this.hideTable = true;
    this.hideDeleteSpinner = true;
    this.service.getDatasets()
      .map(data => data.json())
      .subscribe((response) => {
        this.hideTable = false;
        this.datasets = response._embedded.metadatas;
      }, (err) => {
        this.errorText = "No data!!!"
      })
  }
  onListColumns(dname) {
    console.log(dname);
    this.modalHeader = dname;
    this.service.getColumns(dname).subscribe(
      (res) => {
        console.log(res.ok)
        if (res.ok) {
          console.log("Get col success");
          let temp = res.json();
          console.log(temp._embedded.metadataCols);
          this.columnDetail = temp._embedded.metadataCols;
          console.log(this.columnDetail)
        }
      })
  }

  onDelete(url) {
    this.showMe = url;
    console.log(url);
    this.hideDeleteSpinner = false;
    this.service.deleteDataset(url).subscribe((response) => {
      console.log(response)
      if (response.status == 204) {
        this.hideDeleteSpinner = true;
        alert("Deleted!!");
        this.ngOnInit();
      }
      else {
        alert("Row Not Found!! Please refresh the screen.");
      }
    })
  }
}
